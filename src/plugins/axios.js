import axios from "axios";

const env = import.meta.env.VITE_BASE_URL_PRODUCCTION;

const api = axios.create({
  baseURL: env,
});

api.interceptors.request.use(
  function (config) {
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

api.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default api;
