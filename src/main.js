import { createApp } from "vue";
import App from "./App.vue";
import "./assets/css/main.css";
//VeeValidate
import { Field, Form, defineRule, configure, ErrorMessage } from "vee-validate";
import { required, email } from "@vee-validate/rules";
//FontAwesome
import { library } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { fab } from "@fortawesome/free-brands-svg-icons";
//Router
import router from "./router";
//PrimeVue
import PrimeVue from "primevue/config";
import "primeicons/primeicons.css";
import "primevue/resources/themes/aura-light-green/theme.css";
import Timeline from "primevue/timeline";
import Button from "primevue/button";
import Card from "primevue/card";
import Image from "primevue/image";
import Dialog from "primevue/dialog";
import InputText from "primevue/inputtext";
import Avatar from "primevue/avatar";
import FloatLabel from "primevue/floatlabel";
import Dropdown from "primevue/dropdown";
import InputGroup from "primevue/inputgroup";
import InputGroupAddon from "primevue/inputgroupaddon";
import Textarea from "primevue/textarea";
import Checkbox from "primevue/checkbox";
import Lara from "../src/presets/lara";
import api from "@/plugins/axios";
import VueAxios from "vue-axios";

//Reglas VeeValidate
defineRule("required", required);
defineRule("email", email);

// Configurar mensajes de error globalmente
configure({
  generateMessage: (context) => {
    return "!";
  },
});

//Iconos FontAwesome
library.add(fas, fab);

createApp(App)
  .use(VueAxios, api)
  .component("font-awesome-icon", FontAwesomeIcon)
  .use(PrimeVue, {
    pt: Lara,
  })
  .component("Timeline", Timeline)
  .component("Button", Button)
  .component("Card", Card)
  .component("Image", Image)
  .component("Dialog", Dialog)
  .component("InputText", InputText)
  .component("Avatar", Avatar)
  .component("FloatLabel", FloatLabel)
  .component("Dropdown", Dropdown)
  .component("InputGroup", InputGroup)
  .component("InputGroupAddon", InputGroupAddon)
  .component("Textarea", Textarea)
  .component("Checkbox", Checkbox)
  .component("Field", Field)
  .component("Form", Form)
  .component("ErrorMessage", ErrorMessage)
  .use(router)
  .mount("#app");
