import { createRouter, createWebHistory } from "vue-router";
import DefaultLayout from "@/layouts/DefaultLayout.vue";
import Home from "@/views/Home.vue";
import QuienesSomos from "@/views/QuienesSomos.vue";
import CanalesPQRSF from "@/views/CanalesPQRSF.vue";
import CanalesCitas from "@/views/CanalesCitas.vue";
import Blog from "@/views/Blog.vue";
import BlogDetails from "@/views/BlogDetails.vue";
import Institucional from "@/views/Institucional.vue";
import Resultados from "@/views/Resultados.vue";
import ServiciosHome from "@/views/Servicios.vue";
import MedicinaEspecializada from "@/views/MedicinaEspecializada.vue";
import Contacto from "@/views/Contacto.vue";
import NuestraHistoria from "@/views/NuestraHistoria.vue";
import MisionVision from "@/views/MisionVision.vue";
import Equipo from "@/views/Equipo.vue";
import ValoresCorporativos from "@/views/ValoresCorporativos.vue";
import DerechosDeberes from "@/views/DerechosDeberes.vue";
import InformesPQRFS from "@/views/InformesPQRFS.vue";
import InstanciaParticipacion from "@/views/InstanciaParticipacion.vue";
import PreguntasFrecuentes from "@/views/PreguntasFrecuentes.vue";
import Organigrama from "@/views/Organigrama.vue";
import Objetivo from "@/views/Objetivo.vue";
import PoliticasInstitucionales from "@/views/PoliticasInstitucionales.vue";
import SeguridadPaciente from "@/views/SeguridadPaciente.vue";
import Servicios from "@/views/Services/Servicios.vue";
import JobList from "@/views/JobList.vue";
import Citas from "@/views/CitasView.vue";
import InfoFinView from "@/views/InfoFinView.vue";

const routes = [
  {
    path: "/",
    component: DefaultLayout, // Usa el layout por defecto
    children: [
      {
        path: "",
        component: Home, // Componente de la página de inicio
      },
      {
        path: "/quienes-somos",
        component: QuienesSomos,
      },
      {
        path: "/nuestra-historia",
        component: NuestraHistoria,
      },
      {
        path: "/mision-vision",
        component: MisionVision,
      },
      {
        path: "/derechos-deberes",
        component: DerechosDeberes,
      },
      {
        path: "/institucional",
        component: Institucional,
      },
      {
        path: "/servicios",
        component: ServiciosHome,
      },
      {
        path: "/canales-PQRSF",
        component: CanalesPQRSF,
      },
      {
        path: "/canales-citas",
        component: CanalesCitas,
      },
      {
        path: "/informes-PQRSF",
        component: InformesPQRFS,
      },
      {
        path: "/instancia-participacion",
        component: InstanciaParticipacion,
      },
      {
        path: "/informacion-financiera",
        component: InfoFinView,
      },
      {
        path: "/blog",
        component: Blog,
      },
      {
        path: "/blog-details/:id",
        component: BlogDetails,
      },
      {
        path: "/consultar-resultados",
        component: Resultados,
      },
      {
        path: "/medicina-especializada",
        component: MedicinaEspecializada,
      },
      {
        path: "/contactanos",
        component: Contacto,
      },
      {
        path: "/nuestro-equipo",
        component: Equipo,
      },
      {
        path: "/valores-corporativos",
        component: ValoresCorporativos,
      },
      {
        path: "/politicas-Institucionales",
        component: PoliticasInstitucionales,
      },
      {
        path: "/seguridad-paciente",
        component: SeguridadPaciente,
      },
      {
        path: "/preguntas-frecuentes",
        component: PreguntasFrecuentes,
      },
      {
        path: "/organigrama",
        component: Organigrama,
      },
      {
        path: "/objetivos",
        component: Objetivo,
      },
      {
        path: "/vacantes",
        component: JobList,
      },
      {
        path: "/citas",
        component: Citas,
      },
      {
        path: "/servicio/:id",
        component: Servicios,
      },
      // Agrega más rutas aquí si es necesario
    ],
  },
  // Agrega más rutas aquí si es necesario
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
